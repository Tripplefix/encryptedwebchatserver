package webchat.Services;

import com.corundumstudio.socketio.*;
import com.corundumstudio.socketio.listener.ConnectListener;
import com.corundumstudio.socketio.listener.DataListener;
import com.corundumstudio.socketio.listener.DisconnectListener;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.gson.Gson;
import webchat.Models.Message;
import webchat.Models.WsModels.LoginRequest;
import java.util.UUID;

public class SocketService {
    private SocketIOServer server;
    private BiMap<String, UUID> connectedUsers;

    public SocketService() {

        connectedUsers = HashBiMap.create();

        Configuration config = new Configuration();
        config.setPort(9092);
        config.setAuthorizationListener(data -> {
            Gson gson = new Gson();
            LoginRequest request =
                    gson.fromJson(data.getSingleUrlParam("data"), LoginRequest.class);

            if(request.getType().equals("login")){

                String token = request.getToken().getTokenString();
                String username = request.getToken().getUsername();

                return JWTService.validateJWT(token, username);
            }
            return false;
        });

        server = new SocketIOServer(config);
        server.addConnectListener(onConnect);
        server.addDisconnectListener(onDisconnect);
        server.addEventListener("enterChatRoom", String.class, onEnterChatRoom);
        server.addEventListener("message", String.class, onMessage);

        server.start();
    }

    private ConnectListener onConnect = client -> {
        System.out.println("Someone connected!");
    };

    private DisconnectListener onDisconnect = client -> {
        System.out.println("Someone disconnected :-(");
        connectedUsers.inverse().remove(client.getSessionId());
    };

    private DataListener<String> onMessage = (client, data, ackRequest) -> {
        Gson gson = new Gson();
        Message msg = gson.fromJson(data, Message.class);

        UUID recipientID = connectedUsers.get(msg.getRecipient());

        if(recipientID != null){
            SocketIOClient recipientClient = server.getClient(recipientID);
            recipientClient.sendEvent("message", gson.toJson(msg));
        }else{
            client.sendEvent("message", "offline");
        }
    };

    private DataListener<String> onEnterChatRoom = (client, data, ackRequest) -> {
        Gson gson = new Gson();
        LoginRequest request =
                gson.fromJson(data, LoginRequest.class);

        String token = request.getToken().getTokenString();
        String username = request.getToken().getUsername();

        if(JWTService.validateJWT(token, username)){
            connectedUsers.putIfAbsent(username, client.getSessionId());
        }
    };
}
