package webchat.Services;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.crypto.MacProvider;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import webchat.Models.WebChatContext;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;

public class JWTService {
    private static Connection connection = WebChatContext.getInstance();

    public static String generateJWT(String username) throws SQLException {

        //The JWT signature algorithm we will be using to sign the token
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        //We will sign our JWT with our ApiKey secret
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(generateSecret(username));
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        //Let's set the JWT Claims
        JwtBuilder builder = Jwts.builder()
                .setIssuedAt(now)
                .setSubject(username)
                .signWith(signatureAlgorithm, signingKey);

        //set expiration
        long expMillis = nowMillis + 10 * 60000;
        Date exp = new Date(expMillis);
        //builder.setExpiration(exp);

        //Builds the JWT and serializes it to a compact, URL-safe string
        return builder.compact();
    }

    public static boolean validateJWT(String jwt, String username)  {
        try {
            return Jwts.parser()
                    .setSigningKey(DatatypeConverter.parseBase64Binary(getSecretByUsername(username)))
                    .parseClaimsJws(jwt)
                    .getBody().getSubject().equals(username);
        } catch (Exception e) {
            return false;
        }
    }

    private static String getSecretByUsername(String username) throws SQLException {

        ScalarHandler<String> sh = new ScalarHandler<>();
        QueryRunner runner = new QueryRunner();
        String query = "SELECT login_token FROM users WHERE username = ?";
        return runner.query(connection, query, sh, username);
    }

    private static String generateSecret(String username) throws SQLException{
        Key key = MacProvider.generateKey();
        String keystring = key.getEncoded().toString();

        QueryRunner runner = new QueryRunner();
        String updateSQL
                = "UPDATE users SET login_token = ? WHERE username = ?";
        int numRowsUpdated = runner.update(connection, updateSQL, keystring, username);

        if(numRowsUpdated == 1){
            return keystring;
        }else{
            throw new SQLException("No rows were updated!");
        }
    }
}
