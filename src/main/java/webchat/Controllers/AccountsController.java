package webchat.Controllers;

import com.google.gson.Gson;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import spark.Request;
import spark.Response;
import spark.Route;

import java.sql.SQLException;
import java.util.*;

import org.apache.commons.dbutils.QueryRunner;
import webchat.Models.User;
import webchat.Models.WsModels.LoginResponse;
import webchat.Services.JWTService;
import webchat.Utils.PasswordAuthentication;

public class AccountsController extends BaseController {

    public static Route handleLogin = (Request request, Response response) -> {

        String currentUser = request.session(true).attribute("currentUser");
        if (currentUser != null){
            request.session().removeAttribute("currentUser");
        }

        Gson gson = new Gson();
        User user = gson.fromJson(request.body(), User.class);

        try	{
            ScalarHandler<String> sh = new ScalarHandler<>();

            QueryRunner runner = new QueryRunner();
            String query = "SELECT password FROM users WHERE username = ?";
            String password
                    = runner.query(connection, query, sh, user.getUsername());

            if(password != null && !password.isEmpty()){
                PasswordAuthentication pa = new PasswordAuthentication();

                // authenticate user
                if(pa.authenticate(user.getPassword().toCharArray(), password)){
                    response.status(200);

                    request.session().attribute("currentUser", user.getUsername());

                    try{
                        // create JWT for user. It will be used to authorize the websocket connection
                        String token = JWTService.generateJWT(user.getUsername());

                        // return token and list of friends
                        return new LoginResponse(
                                true,
                                "login successful",
                                token,
                                getFriends(user.getUsername())).toJson();
                    }catch (Exception ex){
                        return "failed: " + ex.getMessage();
                    }
                }else{
                    response.status(401);
                    return "wrong password for user: " + user.getUsername();
                }
            }else{
                response.status(401);
                return "user not found: " + user.getUsername();
            }
        } catch(SQLException sqle) {
            throw new RuntimeException("Problem updating", sqle);
        }
    };

    public static Route handleLogout = (Request request, Response response) -> {
        response.status(200);

        request.session().removeAttribute("currentUser");
        request.session().attribute("loggedOut", true);

        return "you have been logged out";
    };

    public static Route handleRegistration = (Request request, Response response) -> {

        Gson gson = new Gson();
        User user = gson.fromJson(request.body(), User.class);

        QueryRunner runner = new QueryRunner();
        String insertSQL
                = "INSERT INTO users (username, password) "
                + "VALUES (?, ?)";

        String passwordHash = new PasswordAuthentication().hash(user.getPassword().toCharArray());

        int numRowsInserted
                = runner.update(
                connection, insertSQL, user.getUsername(), passwordHash);

        if(numRowsInserted == 1){
            response.status(200);
            return "registration successful";
        }else{
            response.status(500);
            return "registration failed";
        }

    };

    private static List<String> getFriends(String username) throws SQLException{
        QueryRunner run = new QueryRunner();
        MapListHandler beanListHandler = new MapListHandler();
        BeanListHandler<String> u = new BeanListHandler<>(String.class);

        List<Map<String, Object>> entries = run.query(connection,
                "SELECT * FROM user_relationships " +
                        "WHERE sender = ? ", beanListHandler, username);

        List<String> friendLists = new ArrayList<>();

        for(Map<String, Object> entry : entries){
            friendLists.add(entry.get("recipient").toString());
        }

        return friendLists;
    }
}
