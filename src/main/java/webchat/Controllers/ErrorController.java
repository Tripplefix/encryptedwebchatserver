package webchat.Controllers;

import spark.Request;
import spark.Response;
import spark.Route;

public class ErrorController {
    public static Route badRequest = (Request request, Response response) -> {
        response.status(404);

        // not found

        return "not found";
    };

    public static Route unauthorized = (Request request, Response response) -> {
        response.status(403);

        // unauthorized

        return "unauthorized";
    };

    public static Route serverError = (Request request, Response response) -> {
        response.status(500);

        // serverError

        return "serverError";
    };
}
