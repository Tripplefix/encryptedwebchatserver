package webchat;

import webchat.Controllers.AccountsController;
import webchat.Controllers.ErrorController;
import webchat.Services.SocketService;
import webchat.Utils.*;

import static spark.Spark.*;

public class Application {

    public static void main(String[] args) {
        new SocketService();
        port(4567);
        setRoutes();
    }

    private static void setRoutes(){
        options("/*",
                (request, response) -> {

                    String accessControlRequestHeaders = request
                            .headers("Access-Control-Request-Headers");
                    if (accessControlRequestHeaders != null) {
                        response.header("Access-Control-Allow-Headers",
                                accessControlRequestHeaders);
                    }

                    String accessControlRequestMethod = request
                            .headers("Access-Control-Request-Method");
                    if (accessControlRequestMethod != null) {
                        response.header("Access-Control-Allow-Methods",
                                accessControlRequestMethod);
                    }

                    return "OK";
                });

        before((request, response) -> response.header("Access-Control-Allow-Origin", "*"));
        before((request, response) -> response.header("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS"));
        before((request, response) -> response.header("Access-Control-Allow-Headers", "Origin, Content-Type, X-Auth-Token"));

        // add trailing slashes
        before("*",             Helpers.addTrailingSlashes);

        // POST-requests
        post(Path.Web.LOGIN,         AccountsController.handleLogin);
        post(Path.Web.LOGOUT,        AccountsController.handleLogout);
        post(Path.Web.REGISTER,      AccountsController.handleRegistration);

        // handle bad requests
        notFound(ErrorController.badRequest);

        // gzip everything
        after((request, response) -> {
            response.header("Content-Encoding", "gzip");
        });
    }
}