package webchat.Utils;

public class Path {
    public static class Web {
        public static final String LOGIN = "/login/";
        public static final String LOGOUT = "/logout/";
        public static final String REGISTER = "/register/";
    }
}