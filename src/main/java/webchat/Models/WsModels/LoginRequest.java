package webchat.Models.WsModels;

import webchat.Models.Token;

public class LoginRequest extends BaseRequest {
    private Token token;

    public LoginRequest(String type, Token token) {
        super(type);
        this.token = token;
    }

    public Token getToken() {
        return token;
    }
}
