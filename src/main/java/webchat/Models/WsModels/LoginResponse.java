package webchat.Models.WsModels;

import com.google.gson.Gson;

import java.util.List;

public class LoginResponse extends BaseResponse {
    private Boolean authenticated;
    private String token;
    private List<String> friends;

    public LoginResponse(Boolean authenticated, String message, String token, List<String> friends) {
        super("login", message);
        this.authenticated = authenticated;
        this.token = token;
        this.friends = friends;
    }

    public LoginResponse(Boolean authenticated, String message) {
        super("login", message);
        this.authenticated = authenticated;
        this.token = null;
        this.friends = null;
    }

    public Boolean getAuthenticated() {
        return authenticated;
    }

    public List<String> getFriends() {
        return friends;
    }

    public String getToken() {
        return token;
    }
}
