package webchat.Models.WsModels;

import com.google.gson.Gson;

public class BaseResponse {
    private String type;
    private String message;

    public BaseResponse(String type, String message) {
        this.type = type;
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    public String toJson(){
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
