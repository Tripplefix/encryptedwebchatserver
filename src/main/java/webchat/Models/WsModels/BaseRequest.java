package webchat.Models.WsModels;

import com.google.gson.Gson;

public class BaseRequest {
    private String type;

    public BaseRequest(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public String toJson(){
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
