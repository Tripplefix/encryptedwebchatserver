package webchat.Models;

import com.mysql.cj.jdbc.MysqlDataSource;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class WebChatContext {

    private static Connection instance = null;
    protected WebChatContext() { }
    public static Connection getInstance() {
        if(instance == null) {
            System.out.println("Connecting database...");
            try{
                String url = "jdbc:mysql://localhost/webchat" +
                        "?useUnicode=true" +
                        "&useJDBCCompliantTimezoneShift=true" +
                        "&useLegacyDatetimeCode=false" +
                        "&serverTimezone=UTC";
                String username = "webchat";
                String password = "LzwnoHwzqIQ9Y12h";

                instance = DriverManager.getConnection(url, username, password);
                System.out.println("Connection successful!");
            } catch (SQLException ex) {
                // handle any errors
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            }
        }
        return instance;
    }
}
