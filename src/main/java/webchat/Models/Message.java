package webchat.Models;

import java.util.Date;

public class Message {
    private String sender;
    private String recipient;
    private String message;
    private String sentAt;

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSentAt() {
        return sentAt;
    }

    public void setSentAt(String sentAt) {
        this.sentAt = sentAt;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String username) {
        this.recipient = username;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Message() { }

    public Message(String sender, String recipient, String message, String sentAt) {
        this.sender = sender;
        this.recipient = recipient;
        this.message = message;
        this.sentAt = sentAt;
    }
}
